#### Overview

RunCloud Credit is credit that you can use to create backup, but cannot be used for the subscription service to RunCloud.

#### Recharge credit

You can top up your credit manually from here. Available amounts starting from $2.50 to $150.00.

#### Auto recharge credit

Here you can enable or disable the auto recharge and set an amount to recharge.
