#### Receipt

All transaction history can be viewed from <a href="https://manage.runcloud.io/settings/payments" target="_blank">here</a>. You will receive a receipt that you can view in your browser and one is sent to you via email. You will receive two emails, one from RunCloud, and the other from our payment processor, BrainTree.