#### Overview

This is your API Key. Please keep this API Key secret. The API Key can be used to download your backup file.

There is a limit to generate an API Key and API Secret. You can generate an API Key or API secret up to **5 times** before you are blocked for 10 minutes from generating another API Key or API Secret.

#### Enable API Access

If you are a developer and want to make use of our API, you can enable API access from [here](https://runcloud.io/docs/api)

#### IP Address Restriction

If you add any IP Address(es) here, any API request using your API Key and API Secret can only be made from the address(es) that you have defined. If you leave this empty, the API request using your key and secret can be made from any IP address.
