#### Log

The account log will display information about any changes you have made to your account, such as modifying your profile and adding or updating your card. It also retains log or your IP address which will display unsuccessful attempts during login.
