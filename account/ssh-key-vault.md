#### Overview

This is where you can add your SSH Key for a later use. The key that you add in here can be used inside [Server SSH Key](https://runcloud.io/docs/guide/server-management/ssh-key).


#### Generating SSH Key

If you do not have any SSH key pair inside your computer, just issue `ssh-keygen -t RSA` inside terminal (Linux & MacOS).

Then you can get the SSH public key by issuing `cat ~/.ssh/id_rsa.pub` which you can register inside RunCloud.

#### Adding SSH Key

To add the SSH Key to your server, you need to fill in the data below:

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Label</td>
            <td>The label for SSH key for you to recognize this key belong to whom</td>
        </tr>
        <tr>
            <td>Public Key</td>
            <td>The SSH public key get from `cat ~/.ssh/id_rsa.pub`</td>
        </tr>
    </tbody>
</table>


#### Deleting SSH Key

When deleting the SSH Key from the vault, you will have an option to delete the same key inside every server containing this key. The checking will be based on the key itself and key comment and label will have no effect.