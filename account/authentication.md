#### Two-Factor Authentication

To setup Two-Factor Authentication, it is recommended that you use Authy, Google Authenticator, or Salesforce Authenticator.

Once you have enabled this step, you may add backup method if you are unable to authenticate via authenticator app. There are two backup authentication methods, Scratch Code and SMS. You can enable both of them as the backup method.

For the Scratch Code, you will be given several codes. You need to store these code somewhere safe. Each Scratch Code can only be used once.

For SMS, you will receive an SMS code to your mobile phone to verify your account.

#### Changing password

In order to update your account with a new password, you need to provide your old password. If you are registered through GitHub, DigitalOcean, or Google authentication, you need to [reset your password](https://manage.runcloud.io/settings/authentication).
