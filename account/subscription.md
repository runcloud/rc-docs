#### Overview

This is the section where you can control your RunCloud subscription. Sign up for our pro plan on a monthly basis!

#### Trial period

Try it before you buy it! RunCloud will give you five days trial period with full features.

#### Paid Plan to Free Plan

You can cancel at any time and you will only be downgraded at the end of you billing date.

#### Free Plan to Paid Plan

Your card will be charged for the subscription immediately for the month or year. Your account should automatically activate all the paid features of your plan. However, if it does not activate automatically, please contact us and we will gladly assist you.

#### Unable to charge payment method

If we are unable to charge your card, your account will be downgraded to the Free Plan automatically.
