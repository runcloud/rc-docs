#### Overview

WordPress Canvas is a WordPress tool inside RunCloud to make life easier. With Canvas, you can predefine how your WordPress installation will look. You customize the themes and plugins to install, permalinks structure, language, and other settings before the WordPress installation.

You may add up to 50 themes and 50 plugins to install. The search feature will make it easier for you to discover themes and plugins from the WordPress repository. You may add up to 20 Canvases per account, but the usage of Canvas is only available for the Business Plan.

To use one of your Canvas, create a One Click WordPress Web Application and choose the Canvas that you want inside the form field. The Canvas installation will take place after you have successfully installed WordPress inside your Web Application.