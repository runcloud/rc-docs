#### Overview

This is where you register your notification integration. Supported notification channels are Email, Slack, and Telegram. You can hook the registered notifications to your **Servers**, **Atomic Deployment**, and **Payment History**.

#### Email

Add your email and we will send the notification to this email address.

#### Slack

You need to register a [Slack Incoming Webhook](https://slack.com/apps/A0F7XDUAZ-incoming-webhooks) to get a WebHook URL.

#### Telegram

You need to talk to [@RunCloudBot](https://t.me/RunCloudBot) to opt-in the notification. You also can invite [@RunCloudBot](https://t.me/RunCloudBot) inside your Telegram Group, Supergroup or Channel.
