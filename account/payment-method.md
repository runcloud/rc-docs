#### Overview

You can manage your credit/debit card or PayPal from here. Please note that **we will never store** your credit card number, expiration date or CVV, as everything is taken care by our payment gateway.

#### Add a new card

When you add or update your card number, you will be charged \$1 for verification. The amount will be returned as soon as verification is complete. Adding or updating card will have no effect on your plan. We will not charge you for adding or updating your card.

#### Add PayPal account

Upon adding a PayPal account, you will not be charged anything until you subscribe to a paid plan. Changing your PayPal account to another account will downgrade your account to the Free Plan, but you can always subscribe back anytime and we will not charge you until end of billing period.

#### Changing from Credit Card to PayPal or vice versa

Changing your payment method will downgrade your account to the Free Plan. After you have changed your payment method, you can subscribe back to a paid plan and we will not charge you until end of billing period.
