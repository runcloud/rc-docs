RunCloud is a hosted search engine capable of delivering real-time results from the first keystroke. RunCloud's powerful API lets you quickly and seamlessly implement search within your websites, mobile, and voice applications. Our search engine powers billions of queries for thousands of companies every month, delivering relevant results in under 100ms anywhere in the world.

The first step is to send your data to RunCloud, in a proper format. Once on RunCloud's servers, you can start testing our search functionality with your own data. You can do this without code, using the dashboard, or with code, using one of our API clients. At this stage, you've got the basic structure of an RunCloud search solution.

There's much more to creating a full search solution, and our docs will walk you through every possibility. But first, why not see RunCloud in action?

#### [](#the-engine) The Engine

RunCloud is a hosted search engine capable of delivering real-time results from the first keystroke. RunCloud's powerful API lets you quickly and seamlessly implement search within your websites, mobile, and voice applications. Our search engine powers billions of queries for thousands of companies every month, delivering relevant results in under 100ms anywhere in the world.

The first step is to send your data to RunCloud, in a proper format. Once on RunCloud's servers, you can start testing our search functionality with your own data. You can do this without code, using the dashboard, or with code, using one of our API clients. At this stage, you've got the basic structure of an RunCloud search solution.

There's much more to creating a full search solution, and our docs will walk you through every possibility. But first, why not see RunCloud in action?

```bash
# Start
systemctl start php70rc-fpm

# Stop
systemctl stop php70rc-fpm

# Reload
systemctl reload php70rc-fpm

# Restart
systemctl restart php70rc-fpm

# Automatically start php after server reboot
systemctl enable php70rc-fpm

# Disable php automatic start after reboot
systemctl disable php70rc-fpm
```

RunCloud is a hosted search engine capable of delivering real-time results from the first keystroke. RunCloud's powerful API lets you quickly and seamlessly implement search within your websites, mobile, and voice applications. Our search engine powers billions of queries for thousands of companies every month, delivering relevant results in under 100ms anywhere in the world.

The first step is to send your data to RunCloud, in a proper format. Once on RunCloud's servers, you can start testing our search functionality with your own data. You can do this without code, using the dashboard, or with code, using one of our API clients. At this stage, you've got the basic structure of an RunCloud search solution.

There's much more to creating a full search solution, and our docs will walk you through every possibility. But first, why not see RunCloud in action?

#### [](#revoke-an-api-key) Revoke an API Key

Revoking an API key will instantly make it unusable. It's important to revoke a key in a case where it may have been compromised (a write-API key leaked, a search-API key is being abused, …), but keep in mind that you need to plan updates for your application accordingly to avoid breaking it when the key becomes invalid.

##### [](#regular-api-key) Regular API Key

You can revoke an API key by deleting it from the [dashboard](#the-engine), or through the API:

```javascript
// Delete any key with the client
client.deleteApiKey('7f2615414bc619352459e09895d2ebda')

// Delete an index-specific key
index.deleteApiKey('9b9335cb7235d43f75b5398c36faabcd')
```
