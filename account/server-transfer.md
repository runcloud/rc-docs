#### Overview

This is where you can accept or reject server transfer request. A Server Transfer Request must be made from an account with with a server to transfer. Transfer Requests can be made from the [Server Setting](https://runcloud.io/docs/guide/server-management/server-setting#transfer-server) page. Old owners may cancel the transfer request before it is accepted and new owners can accept or reject the request.

[Slack Notification](https://runcloud.io/docs/guide/server-management/notification#usage), [Telegram Notification](https://runcloud.io/docs/guide/server-management/notification#usage), [Health Notification](https://runcloud.io/docs/guide/server-management/notification#health-notification-setting) and [Backup Instance](https://runcloud.io/docs/backup/summary#rc-docs-scroll) must be setup again for the new server.
