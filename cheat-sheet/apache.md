#### Basic command

Since we are rolling our own Apache, we changed the `Apache` name to prevent conflict from the main Ubuntu Repository. We named our Apache to `apache2-rc`.

<div class="alert alert-danger">You don't have to run any of these commands if you are managing your server through RunCloud Panel</div>

```bash
# Start
systemctl start apache2-rc

# Stop
systemctl stop apache2-rc

# Reload
systemctl reload apache2-rc

# Restart
systemctl restart apache2-rc

# Start apache2 after reboot
systemctl enable apache2-rc

# Disable apache2 automatic start after reboot
systemctl disable apache2-rc

# Config Test
apachectl -t
```

#### Apache location

```bash
# Installation location
/RunCloud/Packages/apache2-rc

# Config location
/etc/apache2-rc/httpd.conf
```

#### Web application config

<div class="alert alert-danger">Do not change this config manually since it will be overwritten by RunCloud</div>

```bash
# If your web application name is myapp, config will resides inside
/etc/apache2-rc/conf.d/myapp.conf
```

#### Multi-Processing Modules (MPM)

By default, RunCloud uses **event** to make use of multiple threads and create multiple task at once. If **event** doesn't work with your configuration, you can change it to **worker**.

```bash
# Edit config file
vi /etc/apache2-rc/httpd.conf
```

You will find these lines:

```apache
#
# Dynamic Shared Object (DSO) MPM
#
LoadModule mpm_event_module modules/mod_mpm_event.so
#LoadModule mpm_worker_module modules/mod_mpm_worker.so
```

Change to worker

```apache
#
# Dynamic Shared Object (DSO) MPM
#
#LoadModule mpm_event_module modules/mod_mpm_event.so
LoadModule mpm_worker_module modules/mod_mpm_worker.so
```

After that, save your apache config and restart apache2 server:

```bash
systemctl restart apache2-rc
```

To make sure your apache2 server successfully run as **worker** MPM, you can use this command to check Server MPM:

```shell
runcloud@testserver:~# httpd -V
Server version: Apache/2.4.29 (Unix)
Server built:   Dec 22 2017 22:11:48
Server's Module Magic Number: 20120211:68
Server loaded:  APR 1.6.3, APR-UTIL 1.6.1
Compiled using: APR 1.6.3, APR-UTIL 1.6.1
Architecture:   64-bit
Server MPM:     worker
  threaded:     yes (fixed thread count)
    forked:     yes (variable process count)
Server compiled with....
 -D APR_HAS_SENDFILE
 -D APR_HAS_MMAP
 -D APR_HAVE_IPV6 (IPv4-mapped addresses enabled)
 -D APR_USE_SYSVSEM_SERIALIZE
 -D APR_USE_PTHREAD_SERIALIZE
 -D SINGLE_LISTEN_UNSERIALIZED_ACCEPT
 -D APR_HAS_OTHER_CHILD
 -D AP_HAVE_RELIABLE_PIPED_LOGS
 -D DYNAMIC_MODULE_LIMIT=256
 -D HTTPD_ROOT="/RunCloud/Packages/apache2-rc"
 -D SUEXEC_BIN="/RunCloud/Packages/apache2-rc/bin/suexec"
 -D DEFAULT_PIDLOG="logs/httpd.pid"
 -D DEFAULT_SCOREBOARD="logs/apache_runtime_status"
 -D DEFAULT_ERRORLOG="logs/error_log"
 -D AP_TYPES_CONFIG_FILE="/etc/apache2-rc/mime.types"
 -D SERVER_CONFIG_FILE="/etc/apache2-rc/httpd.conf"
```
