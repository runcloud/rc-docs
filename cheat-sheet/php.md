#### Naming convention

Our PHP version naming depending on the name shown below

```bash
# PHP Name
php[VERSION]rc

# PHP FPM name
php[VERSION]rc-fpm
```

The **VERSION** are 55, 56, 70, 71, 72 and so on depending on php version 5.5, 5.6, 7.0, 7.1 and 7.2 respectively.

#### Basic command

<div class="alert alert-danger">You don't have to do any of this command if you are changing PHP Config from RunCloud Panel</div>

If you would like to start, stop, reload or restart php7.0, you may use:

```bash
# Start
systemctl start php70rc-fpm

# Stop
systemctl stop php70rc-fpm

# Reload
systemctl reload php70rc-fpm

# Restart
systemctl restart php70rc-fpm

# Automatically start php after server reboot
systemctl enable php70rc-fpm

# Disable php automatic start after reboot
systemctl disable php70rc-fpm
```

Or to start, stop, reload or restart php7.1, you can use:

```bash
# Start
systemctl start php71rc-fpm

# Stop
systemctl stop php71rc-fpm

# Reload
systemctl reload php71rc-fpm

# Restart
systemctl restart php71rc-fpm

# Automatically start php after server reboot
systemctl enable php71rc-fpm

# Disable php automatic start after reboot
systemctl disable php71rc-fpm
```

#### Web application location

Each of your web applications will have their own PHP config. They are located inside `/etc/php[VERSION]rc/fpm.d/<web_app_name>.conf`

```bash
# If your web app name is myapp and using php7.0, the config will be located inside
/etc/php70rc/fpm.d/myapp.conf
```

#### PHP location

Each PHP is installed inside its own folder with its own setting folder.

```bash
# Installed location
/RunCloud/Packages/php[VERSION]rc/

# Config location
/etc/php[VERSION]rc/php.ini


# Example
/RunCloud/Packages/php55rc/ # Installed location
/etc/php55rc/php.ini # Config location

/RunCloud/Packages/php56rc/ # Installed location
/etc/php56rc/php.ini # Config location

/RunCloud/Packages/php70rc/ # Installed location
/etc/php70rc/php.ini # Config location

/RunCloud/Packages/php71rc/ # Installed location
/etc/php71rc/php.ini # Config location

/RunCloud/Packages/php72rc/ # Installed location
/etc/php72rc/php.ini # Config location
```

#### Extra PHP-FPM configuration

While most of the config can be tweaked from the panel, some of it isn't supported out of the box. To give you more freedom, we introduced extra PHP-FPM configuration.

You can find your extra PHP-FPM configuration inside `/etc/php-extra/<web_application_name>.conf`.

What you can do with it is that you can add php.ini tweak here. By default, RunCloud depends on php.ini inside `/etc/php<version>rc/php.ini` and overwrite it inside `/etc/php<version>rc/fpm.d/<web_application_name>.conf`. But some of the config isn't supported to be tweak from the panel and will still be rely on php.ini. By modifying `/etc/php<version>rc/fpm.d/<web_application_name>.conf`, each of your web applications will get their own exclusive settings. Editing php.ini is a no-no and will affect every web application globally unless you know what you are doing.

Below example will show you how to use extra fpm configuration.

```ini
; Inside php.ini, there is a setting to enable php short tag.
; Instead of writing <?php you can write <? and php will execute.
; The default value is disabled.
; To turn it on globally, you can just edit php.ini
short_open_tag = On
```

But if you are **not** going to use this globally, you should add it inside extra php-fpm configuration. For example, my Web Application name is **awesomephp**. The extra configuration file will be located inside `/etc/php-extra/awesomephp.conf`. Edit the file by using configuration below and save it.

```ini
; This will be exclusive to awesomephp Web Application only
php_admin_value[short_open_tag] = On
```

The setting style is clear. You take whatever key you wanted to modified inside php.ini, wrap it with php_admin_value and assign the value you wanted to overwrite.

```ini
php_admin_value[key] = value
```

After you have edited your PHP-FPM extra configuration file, reload your PHP-FPM

```bash
# systemctl reload php<version>rc-fpm
# If you are using PHP 7.1 for awesomephp Web Application
systemctl reload php71rc-fpm
```

This idea was coming from our customers who are using Wordfence plugin for WordPress. We also have dedicated post for this inside our blog, <a href="https://blog.runcloud.io/2017/10/01/wordfence-integration-with-php-fpm-inside-runcloud.html" target="_blank">Wordfence Integration with PHP-FPM inside RunCloud</a>.

#### ionCube loader

To install ionCube Loader, please follow this guide inside our [blog](https://blog.runcloud.io/how-to-install-ioncube-loader-to-runcloud-server).

#### Zend Guard loader

To install Zend Guard Loader, please follow this guide inside our [blog](https://blog.runcloud.io/how-to-install-zend-guard-loader-inside-runcloud-server).

#### Installing modules

This is the list of extra module if you want to use it:

<table class="table table-hover table-bordered table-striped">
    <thead>
        <tr>
            <th>Module Name</th>
            <th>Package Name</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>imap</td>
            <td>
                php55rc-imap <br>
                php56rc-imap <br>
                php70rc-imap <br>
                php71rc-imap <br>
                php72rc-imap <br>
            </td>
            <td>The module to access IMAP. Most probably you are not using this module, unless you have a Web Application that can read email.</td>
        </tr>
        <tr>
            <td>ldap</td>
            <td>
                php55rc-ldap <br>
                php56rc-ldap <br>
                php70rc-ldap <br>
                php71rc-ldap <br>
                php72rc-ldap <br>
            </td>
            <td>The module to access LDAP (Lightweight Directory Access Protocol).</td>
        </tr>
        <tr>
            <td>geoip</td>
            <td>
                php55rc-pecl-geoip <br>
                php56rc-pecl-geoip <br>
                php70rc-pecl-geoip <br>
                php71rc-pecl-geoip <br>
                php72rc-pecl-geoip <br>
            </td>
            <td>The module to work with MaxMix GeoIP. This module does not work with GeoIP2.</td>
        </tr>
        <tr>
            <td>imagick</td>
            <td>
                php55rc-pecl-imagick <br>
                php56rc-pecl-imagick <br>
                php70rc-pecl-imagick <br>
                php71rc-pecl-imagick <br>
                php72rc-pecl-imagick <br>
            </td>
            <td>The module for image processing using ImageMagick.</td>
        </tr>
        <tr>
            <td>mongodb</td>
            <td>
                php55rc-pecl-mongodb <br>
                php56rc-pecl-mongodb <br>
                php70rc-pecl-mongodb <br>
                php71rc-pecl-mongodb <br>
                php72rc-pecl-mongodb <br>
            </td>
            <td>MongoDB database driver for PHP.</td>
        </tr>
        <tr>
            <td>pgsql</td>
            <td>
                php55rc-pgsql <br>
                php56rc-pgsql <br>
                php70rc-pgsql <br>
                php71rc-pgsql <br>
            </td>
            <td>The driver to work with PgSQL Database.</td>
        </tr>
        <tr>
            <td>snmp</td>
            <td>
                php55rc-snmp <br>
                php56rc-snmp <br>
                php70rc-snmp <br>
                php71rc-snmp <br>
                php72rc-snmp <br>
            </td>
            <td>The module to manage remote device. Simple Network Management Protocol.</td>
        </tr>
        <tr>
            <td>soap</td>
            <td>
                php55rc-soap <br>
                php56rc-soap <br>
                php70rc-soap <br>
                php71rc-soap <br>
                php72rc-soap <br>
            </td>
            <td>The module to communicate with SOAP server or to built your own SOAP server.</td>
        </tr>
    </tbody>
</table>

```bash
# To install any module, just use the module name
# This example will install soap module to php5.5, php5.6, php7.0, php7.1 php7.2
apt-get install php55rc-soap php56rc-soap php70rc-soap php71rc-soap php72rc-soap

# Then reload the php
systemctl reload php55rc-fpm
systemctl reload php56rc-fpm
systemctl reload php70rc-fpm
systemctl reload php71rc-fpm
systemctl reload php72rc-fpm
```

#### Installing custom PHP modules

Since we are rolling our own PHP Version, apt-get install php-\* won't work because it will replace our php and there will be a conflict. Thus, you need to compile your own modules to make it work. Don't worry about it because most modules already shipped with RunCloud except you are trying to do some weird things inside your server.

This example will teach you how to compile <a href="https://pecl.php.net/package/sphinx" target="_blank" rel="nofollow">pecl-sphinx</a> inside your RunCloud Server.

You need to login as **root** user to be able to compile and install the module.

```bash
# Install the required developement tools
apt-get install autoconf libpcre3-dev

# Set module name
MODULE_NAME="sphinx"

# Set download version
MODULE_VERSION="1.3.3"

# Download & Extract
cd ~
wget https://pecl.php.net/get/$MODULE_NAME-$MODULE_VERSION.tgz
tar -zxvf $MODULE_NAME-$MODULE_VERSION.tgz
cd $MODULE_NAME-$MODULE_VERSION


# Installing for PHP5.5
# make clean will always fail if you never compile it before
make clean

/RunCloud/Packages/php55rc/bin/phpize --clean

/RunCloud/Packages/php55rc/bin/phpize

./configure --with-libdir=lib64 CFLAGS='-O2 -fPIE -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -fsigned-char -fno-strict-aliasing'

make install

echo "extension=$MODULE_NAME.so" > /etc/php55rc/conf.d/$MODULE_NAME.ini

systemctl restart php55rc-fpm


# Installing for PHP5.6
# make clean will always fail if you never compile it before
make clean

/RunCloud/Packages/php56rc/bin/phpize --clean

/RunCloud/Packages/php56rc/bin/phpize

./configure --with-libdir=lib64 CFLAGS='-O2 -fPIE -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -fsigned-char -fno-strict-aliasing'

make install

echo "extension=$MODULE_NAME.so" > /etc/php56rc/conf.d/$MODULE_NAME.ini

systemctl restart php56rc-fpm
```
