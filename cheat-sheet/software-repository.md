#### Overview

RunCloud rolls its own custom software for NGiNX, Apache2, and PHP but **does not** change any core code.

The RunCloud software is installed with a different configuration from the Ubuntu standard repository and is crucial to allow us to tweak and modify each software configuration. For example, the standard Ubuntu repository will prevent you from installing PHP5.5 alongside PHP5.6. The only way to achieve this is to compile your own PHP and that is what we are doing. So you can use multiple PHP version inside your server(s) depending on your web application needs.
