#### General

These are the general settings for your Atomic Git Deployment project. Inside here, you can enable or disable automatic deployment when you push your code. You may also set whether you want to enable composer install or run composer install with dev dependencies. Besides that, you can enable deployment notifications for successful or failed deployments.

#### Notification

This is where to hook your notification channel to the deployment. You can select either Email, Slack, or Telegram notifications. All of this must be registered inside  [Notification Channel](https://runcloud.io/docs/guide/account/notification-channel) in Account Settings.

#### Git

This is where you can update your Git configuration. If you wish to deploy a different branch or move to a new repository, this is where you do that. The changes will only be reflected in the next deployment.
