#### Symlink

Inside the Symlink menu, you can add your own symlinks. There are **two** types of symlink, **Config** and **Directory**.

#### Config

This is where you should add your config file(s). Usually, config file(s) are not tracked by git. For example, with Laravel and Symfony you have `.env` config files. Let's say you name it `application-env`. Inside the link from field, you should add `application-env` and the link to field, `.env`. The name of the file inside the configs folder doesn't matter, but in the link to field, you should name it with the correct name for your application.

When you create a config file, you need to choose which Web Application will receive this config. Most of the time, you need to choose all Web Applications, unless each Web Application uses different configs.

The config that you create will be encrypted. A secret key must be provided when you want to install or update the config file. If you forget the secret key the only thing you can do is delete it and add it again. Deleting the config file will not delete it inside your server.

If the config you are trying to link already **exists** inside your code, the deployment will stop with an error.

#### Directory

This is where you should link your uploaded files. For example, you have a folder named uploads inside your Web Application. This uploads folder should be empty inside your code. You can instruct this uploads folder to symlink to the storage directory. Thus, every deployment will automatically link to the storage directory to get the uploaded files.

If the folder already exists inside your code, we will **replace** it automatically and the deployment will continue without error.

#### Linking the Configs and Directories

Linking of the newly added configs and directories only happens on the next deployment. If you don't have any new code to push, you can just the click Force Deployment button.
