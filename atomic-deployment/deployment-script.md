#### Overview

As described inside the [Getting Started](https://runcloud.io/docs/guide/atomic-deployment/getting-started) section, Atomic deployment is divided into **three tasks**, **_Cloning a new release_**, **_installing Composer dependencies_** and **_activating the latest release_**. These are the basic tasks required to get your site running. But in real world usage, you will have plenty of tasks to run before completing the deployment.

You can add to and extend the deployment tasks by including new tasks in the deployment script. Your tasks can either be done before or after then main task.

#### Cloning a new release

This is where the deployment script will run a clone of your latest code. After the successful clone, we will try to link up the symlinks that you have provided inside the [symlink](https://runcloud.io/docs/guide/atomic-deployment/symlink) menu.

#### Installing Composer dependencies

This task will install composer dependencies. [General Setting](https://runcloud.io/docs/guide/atomic-deployment/settings#general) allows you to control whether you want to run a composer installation or not, or you can even opt for installing dev dependencies.

#### Activating the latest release

After all the other tasks have completed successfully, this task will point your live folder to the latest codebase.

#### Writing your deployment script

When you write a deployment script, you can choose when to run your script based on the three basic tasks. You can place your custom script before or after running the main tasks.

With Atomic Git Deployment, you **don't have to reload** your PHP-FPM to clear the PHP Opcache. We will clear it automatically without reloading your PHP.

#### Replacer

For a convenience in writing the deployment script, we provide you with a Replacer.

<table class="table table-bordered">
    <tbody>
        <tr>
            <td><code>{ROOTPATH}</code></td>
            <td>Path to /home/&lt;username&gt;/webapps/&lt;web app name&gt;</td>
        </tr>
        <tr>
            <td><code>{RELEASE}</code></td>
            <td>The current release version being deployed</td>
        </tr>
        <tr>
            <td><code>{RELEASEPATH}</code></td>
            <td>Path to /home/&lt;username&gt;/webapps/&lt;web app name&gt;/releases/<code>{RELEASE}</code></td>
        </tr>
        <tr>
            <td><code>{CONFIGPATH}</code></td>
            <td>Path to /home/&lt;username&gt;/webapps/&lt;web app name&gt;/configs</td>
        </tr>
        <tr>
            <td><code>{STORAGEPATH}</code></td>
            <td>Path to /home/&lt;username&gt;/webapps/&lt;web app name&gt;/storage</td>
        </tr>
        <tr>
            <td><code>{PHP55}</code></td>
            <td>Run PHP-CLI using /RunCloud/Packages/php55rc/bin/php</td>
        </tr>
        <tr>
            <td><code>{PHP56}</code></td>
            <td>Run PHP-CLI using /RunCloud/Packages/php56rc/bin/php</td>
        </tr>
        <tr>
            <td><code>{PHP70}</code></td>
            <td>Run PHP-CLI using /RunCloud/Packages/php70rc/bin/php</td>
        </tr>
        <tr>
            <td><code>{PHP71}</code></td>
            <td>Run PHP-CLI using /RunCloud/Packages/php71rc/bin/php</td>
        </tr>
        <tr>
            <td><code>{PHP72}</code></td>
            <td>Run PHP-CLI using /RunCloud/Packages/php72rc/bin/php</td>
        </tr>
    </tbody>
</table>

For example this:

```bash
cd {RELEASEPATH}
{PHP72} artisan migrate --force
```

Will be translated to this:

```bash
cd /home/runcloud/webapps/myproject/releases/1536424572396
/RunCloud/Packages/php72rc/bin/php artisan migrate --force
```
