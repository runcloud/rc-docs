#### Web Application

<div class="alert alert-danger">
    Please take note that once you convert your Web Application to use Atomic Git deployment, there is no way to downgrade back to using standard git deployment without deleting the deployment folders.
</div>

This is where you can add a new Web Application with the same Git Repository as the Atomic Git Deployment project. Web Applications can only be added if you are using the same branch of the same git provider service.

#### Delete Web Applications

When you want to delete the Web Application from the project, you can choose between three options. **Delete git settings but keep the data inside the server**, **Delete both git settings and the data inside the server** or **Delete the Web Application completely**.
