#### Overview

<div class="alert alert-danger">
    Please take note that once you convert your Web Application to use Atomic Git Deployment, there is no way to downgrade back to using standard git deployment without deleting the deployment folders.
</div>

If you are using RunCloud powered git deployment, you may notice some web application downtime when your deployment runs `composer install` or npm to build your assets (js and css).

For large sites with thousands of users accessing them concurrently, this will result in a poor user experience that is widespread. Many users will experience your website downtime when all you are doing is upgrading it to a new codebase.

This is where Atomic Git Deployment provides an optimal solution. When you do a git push, your website will be built in the background. All the `composer install` and npm build processes will run inside a separate folder without touching the site that is currently being served. But when the build process is done, it will atomically switch your site to the new codebase without needing any downtime, and provide a seamless experience for your users.

#### Parallel Deployment

When you use Atomic Git Deployment, you can control multiple Web Applications with the same git repository. All deployment will be running at the same time inside every Web Application. Horizontal scaling of your site is no longer a problem when two or more sites on different servers can be built together at the same time. Now all you have to worry about is how to share uploaded files across your network. **\*Hint\*** GlusterFS, shared storage and, as a last resort, Amazon S3.

#### Deployment Tasks

The build process is divided into **three tasks**, **_cloning a new release_**, **_installing Composer dependencies_** and **_activating the latest release_**. The build process will be run by task even though you have multiple Web Applications. Thus, it will wait for every Web Applications to complete it's current task before continuing to the next deployment task.

If your deployment hits an error, the build process will stop and cleanup by itself. Your website will not change to a problematic deployment, thus maintaining the uptime of your site and ensuring a seamless user experience. For parallel Web Application deployment, if one Web Application causes an error, every Web Application will stop from building the next deployment task.

#### Folder Structure

When you upgrade your Web Application to allow Atomic Git Deployment, you will notice the folder structure will change. For example, a Web Application named _myawesomesite_ owned by user _runcloud_ will have a Web Application root path at `/home/runcloud/webapps/myawesomesite`. Then inside this folder, you will have your source code. But inside Atomic Git Deployment, you will have _configs_, _live_, _releases_ and _storage_.

-   <strong>configs</strong> - This is to store your config file(s). E.g, you can store Laravel or Symfony .env or CodeIgniter config.php here.
-   <strong>live</strong> - This is a symlink that points to your latest release.
-   <strong>releases</strong> - Inside here, there are four folders with the latest releases sorted by time of release.
-   <strong>storage</strong> - This is where to store your uploaded files.
