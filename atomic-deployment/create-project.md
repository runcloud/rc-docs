#### Create Atomic Deployment Project

Creating a new Atomic Git Deployment project requires you to select which Web Application you want to convert to Atomic Git Deployment. This will allow us to populate the Git repository you are using.

<div class="alert alert-warning">
    Please take note that once you convert your Web Application to use Atomic Git deployment, there is no way to downgrade back to using standard Git deployment without deleting the deployment folders.
</div>

You **can't** use a Web Application with a custom Git server here, unless you are using Self Hosted GitLab.

After you have successfully created a project, you will notice that your Web Application will have been rebuilt. This is because we have updated your Web Application by prepending `/live` to the public path.

Once the project has been successfully created, you can remove the original WebHook URL that you were given for this Web Application since Atomic Git Deployment will use a newly created WebHook URL.
