#### Overview

This is the main function of our Atomic Git Deployment feature. Inside here, you will be presented with your latest deployment. You will have, at most, four deployment summaries inside.

Each deployment will include a deployment status. There are four statuses available, **Completed**, **Failed**, **Pending** and **Deploying**.

A description of each status is as follows:

-   **Completed** - This deployment was successful and your current Web Application live folder will be using the top most completed deployment.
-   **Failed** - The deployment failed due to an exit with non-zero value.
-   **Pending** - This deployment is waiting for your current deployment to complete.
-   **Deploying** - You are currently deploying this release, the process is ongoing.

#### Forcing Deployment

This will force a deployment using the latest release from the Git branch you are using.

#### Deployment Task Summary

The deployment is divided into three tasks as explained inside the [Getting Started](https://runcloud.io/docs/guide/atomic-deployment/getting-started) section. This is where you view the deployment tasks, including the log for each task for each Web Application.

In real life development, you may have up to seven or more tasks. You may read about this inside the [Deployment Script](https://runcloud.io/docs/guide/atomic-deployment/deployment-script) section.
