#### Summary

This is the summary of your Atomic Git Deployment project. Inside here, you will be given the WebHook URL that you should install inside your Git Repository settings.

You will also gain a total insight of successful and failed deployments.
