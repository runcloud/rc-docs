#### Overview

Archived backup files are backup files that have lost their backup instance. This is due to the deletion of a backup instance or deletion of Web Application or Database. These archived backup files can be downloaded and restored like a normal backup file.

#### Restore backup files

When you click restore backup, you need to choose which Web Application/Database this backup files will be deployed. You can deploy your backup to any Web Application/Database in any server that you own.

If the backup file is a Web Application backup, restoring backup files will **overwrite** your files inside the Web Application if it already exists.

If the backup file is a Database backup, please make sure the selected database is **empty** before restoring your backup. If it isn't, please be sure that you know what you are doing.
