#### Overview

Your RunCloud Agent needs to be at least **Version 2.0.5** to use backup feature. RunCloud Backup does not save your backup files inside your server, but we securely store it inside our cloud storage.

RunCloud Backup gives you 30GB of cloud storage to store your backup and you can directly deploy your backup files inside other server or you can download it to your computer.

With RunCloud Backup, you can backup your **Web Application** or **Database**. You **can't do a server image backup** with RunCloud Backup.

Every backup is stored in **tarball** format (**tar.gz**). If you wanted to deploy the backup file inside a server outside RunCloud, you can download the tarball, upload them to your server and extract them to the intended directory.

RunCloud backup uses [RunCloud Credit](https://runcloud.io/docs/guide/account/credit) to charge the backup instance. The credit charges is flat rate **\$0.03/day** for Pro Subscriber and **\$0.06/day** for Free Subscriber **per backup instance**.

#### Summary

This is the summary of your backup. It will show your current backup storage usage, total Web Applications, and Databases that have been configured to use RunCloud Backup.
