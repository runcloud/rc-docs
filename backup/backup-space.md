#### Backup space

This is where you can upgrade your backup space. Each customers will get **30GB** of backup space. If 30GB is not enough for you, you can upgrade your backup space to maximum **1TB** from here.

Extending your backup space will charge your [RunCloud Credit](https://runcloud.io/docs/guide/account/credit#rc-docs-scroll). This backup space is calculated per daily basis.
