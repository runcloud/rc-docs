#### Overview

This is the place to enable backups for your Web Applications and Databases. To enable or disable your backup, you will need to toggle a button to enable the backup. To view the backup summary, backup settings and backup files, click the view button.

#### Creating backup instance for web application

To create backup instance for a Web Application, there are a few form field that you need to configure.

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>When to Backup</td>
            <td>Select the interval when your backup is going to launch. Minimum interval is every 2 hours and maximum interval is every 7 days.</td>
        </tr>
        <tr>
            <td>Backup Retention</td>
            <td>How long the backup files will be keep. Minimum is 3 days and maximum is 1 month. Your backup data will automatically be deleted depending on this value.</td>
        </tr>
        <tr>
            <td>Success Backup Notification</td>
            <td>If you tick this box, every successful backup will be notified to your email, Slack and/or Telegram (depends on <a role="button" href="/docs/guide/server-management/notification">Server Notification</a> setting).</td>
        </tr>
        <tr>
            <td>Failed Backup Notification</td>
            <td>If you tick this box, every failed backup will be notified to your email, Slack and/or Telegram (depends on <a role="button" href="/docs/guide/server-management/notification">Server Notification</a> setting).</td>
        </tr>
        <tr>
            <td>Backup Type</td>
            <td>You can select full or custom backup. Full backup will backup your entire Web Application. Custom backup will backup list of your chosen files and folders. If you are using git, it is crucial not to backup any file tracked by git.</td>
        </tr>
    </tbody>
</table>

#### Creating backup instance for database

To create backup instance for a Database, there are a few form field that you need to configure.

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>When to Backup</td>
            <td>Select the interval when your backup is going to launch. Minimum interval is every 2 hours and maximum interval is every 7 days.</td>
        </tr>
        <tr>
            <td>Backup Retention</td>
            <td>How long the backup files will be keep. Minimum is 3 days and maximum is 1 month. Your backup data will automatically be deleted depending on this value.</td>
        </tr>
        <tr>
            <td>Success Backup Notification</td>
            <td>If you tick this box, every successful backup will be notified to your email, Slack and/or Telegram (depends on <a role="button" href="/docs/guide/server-management/notification">Server Notification</a> setting).</td>
        </tr>
        <tr>
            <td>Failed Backup Notification</td>
            <td>If you tick this box, every failed backup will be notified to your email, Slack and/or Telegram (depends on <a role="button" href="/docs/guide/server-management/notification">Server Notification</a> setting).</td>
        </tr>
    </tbody>
</table>

#### Backup Configuration

The settings are same as creating a backup instance. Check the field for creating backup instance for Web Application or Database.

#### Backup Files

This is where your backup files will be located after they have has successfully uploaded. You can download the backup files to your computer by clicking on the backup file name. Downloading the files requires your API Key and API Secret which can be found in [API Key](https://runcloud.io/docs/guide/account/api-key) menu.

#### Restore backup files

When you click restore backup, you need to choose which **Web Application / Database** backup files will be deployed. You can deploy your backup to any **Web Application / Database** in any server that you own.

For **Web Application**, restoring backup files will **overwrite** your files inside the Web Application if they already exists.
