#### Version

Your RunCloud server is equipped with multiple PHP Versions starting from PHP Version 5.5. However, your server doesn't know which PHP Version is the default version to use. In this section, you can tell the server which php-cli version to use as default version.

#### Usage

The usage of php-cli is to run php in command line mode such as running in Cron or Supervisord. Composer will also run according to this php-cli version.

<div class="alert alert-danger">If you have problem running <strong>composer</strong> or <strong>php</strong>, most likely you didn't run ssh as <strong>interactive</strong> mode. To fix the problem, just issue below command.
</div>

```bash
source /etc/profile.d/runcloudpath.sh
```
