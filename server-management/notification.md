#### Usage

Notification will be sent with every work that the server has done to your Slack and/or Telegram channel. This includes creating a new web application, adding a new user, and even triggering git webhook.

You need to provide the Slack Webhook URL to push notification to your Slack channel.

To hook your server's notification to Telegram, start a chart with **@RunCloudBot**, then the instruction will be given by the bot.

Slack and/or Telegram notification will also send you the Server Health notification if your server is in bad health.

#### Health notification setting

If you are annoyed by the health notification that we sent to you. You can stop the email, Slack and/or Telegram notification here, but you won't know when your server is really in high usage.
