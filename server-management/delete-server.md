#### Deleting your server

Deleting your server from RunCloud is permanent. You WILL NOT BE ABLE TO RECONNECT it back after you have deleted your server. The only way to reconnect is to format your server and run RunCloud's installation again.

To completely uninstall RunCloud, you must reformat your server. We do not have an uninstallation script.
