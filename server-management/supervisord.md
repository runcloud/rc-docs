#### What is Supervisord?

Supervisord is a client/server system that allows its users to monitor and control a number of processes on UNIX-like operating systems. <a href="http://supervisord.org/#supervisor-a-process-control-system" target="_blank" rel="nofollow"><sup>1</sup></a>

If you have script that running in the background such as a script that is waiting for the user to do something, run it using supervisord. It is different from cron because Supervisord is always running and never stops.

Example scripts that should be running using Supervisord is <a href="https://laravel.com/docs/master/queues" target="_blank" rel="nofollow">Laravel Queue</a> and Nodejs Script.

#### Creating Supervisord job

To create supervisord job in Runcloud, simply click "Create" button and fill in the form.

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Name</td>
            <td>The name of your job</td>
        </tr>
        <tr>
            <td>User</td>
            <td>Username of a user to run the job</td>
        </tr>
        <tr>
            <td>Auto restart</td>
            <td>Enable your job to auto restart if there is an error</td>
        </tr>
        <tr>
            <td>Auto start</td>
            <td>
                Enable your job to automatically start after service is running
            </td>
        </tr>
        <tr>
            <td>Numprocs</td>
            <td>
                Number of processing job to run in parallel
            </td>
        </tr>
        <tr>
            <td>Vendor</td>
            <td>
                Pre-defined binary provider that you want to use to run the job
            </td>
        </tr>
        <tr>
            <td>Command</td>
            <td>
                The actual command/script to run
            </td>
        </tr>
    </tbody>
</table>

Click save button and RunCloud will push your job to your server.

#### Job status

Every job will have their status whether it is **RUNNING** or **FATAL**. RunCloud get this status report from your server in real time.

#### Reloading job

You may reload you job if you change the content of the script that you asked Supervisord to run. This will send the command to the server to reload the job.

#### Deleting job

If you are no longer using the Supervisord job. You may delete the job. Deleting a job will have no effect on other jobs.

#### Rebuild jobs

If you are manually editing Supervisord job, you might want to restore it back to default configuration that is stored in RunCloud. This will revert all Supervisord jobs to the configuration that is stored in RunCloud. Any job that is not created by RunCloud **will not be deleted**.
