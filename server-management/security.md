#### Overview

With RunCloud, every unused port is automatically closed from outside access. The only ports opened are 22/tcp (SSH), 80/tcp (HTTP), 443/tcp (HTTPS), 34210/tcp (RunCloud Communication Port). We are using FirewallD as the firewall and Fail2Ban to block unauthorized attempts to access your server.

#### Fail2Ban

The RunCloud server has been configured to block attempts to access your server using Fail2Ban. However, there is no control inside our panel.

For SSH, any **5 attempts** to access your server using SSH in 10 minute intervals will result in blocking the IP address depending on bantime value inside /etc/fail2ban/jail.local.

For RunCloud Agent (port 34210), any **2 attempts** to access your server's agent without valid serverID and serverKey will be blocked depending on bantime value inside /etc/fail2ban/jail.local.

Blocking with Fail2Ban will only block the desired port. Blocked IP addresses can still access your website as usual.

#### FirewallD

If you need to know more about FirewallD, please refer to their <a href="http://www.firewalld.org/documentation/" target="_blank" rel="nofollow">documentation</a>. However you don't need to know how to setup the firewall because we have already configured the firewall for your server.

#### Global open port rule

When adding a new firewall rule, you can globally open the port for the outside world to access your server. Globally open port rules only require two arguments:

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Port</td>
            <td>The port that you want to globally open to the world</td>
        </tr>
        <tr>
            <td>Protocol</td>
            <td>The protocol either TCP or UDP</td>
        </tr>
    </tbody>
</table>

To accept both TCP and UDP for the same port, add new rule again with the desired protocol.

#### Rich rule

When using rich rule, you can specify to accept or to reject an IP Address or CIDR. Rich rule require four arguments:

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Port</td>
            <td>The port that you want to globally open to the world</td>
        </tr>
        <tr>
            <td>Protocol</td>
            <td>The protocol either TCP or UDP</td>
        </tr>
        <tr>
            <td>IP Address</td>
            <td>IP address or CIDR to accept/reject</td>
        </tr>
        <tr>
            <td>Action</td>
            <td>Action to perform to the IP Address/CIDR</td>
        </tr>
    </tbody>
</table>

To accept/block both TCP and UDP for the same port, add same rule again with the desired protocol.

#### Deploying firewall

After you have added the global open port or rich rule, the firewall is not yet deployed to your server. You have to click deploy firewall button to deploy your firewall. After that, your server will use new firewall rules that you have configured.

#### Unblock IP address

You will be able to unblock any banned IP Address for the SSH port. If you accidentally block yourself, you can always login to the RunCloud Panel and remove your IP Address from the block list. After you have done that, you will be able to SSH inside your server as normal. **Pro tip:** use SSH key.
