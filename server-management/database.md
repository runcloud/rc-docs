#### Database overview

RunCloud uses MariaDB for your database. It is a drop-in replacement for MySQL. Please note that MariaDB == MySQL but MySQL != MariaDB. Every tool created for MySQL is also compatible with MariaDB.

Your database runs in `tcp mode` rather than `sock` so that you can open your firewall port to use your database remotely. By default, we close your database port from the outside. You can enable it by adding firewall to port 3306/tcp inside the  [Security](https://runcloud.io/docs/guide/server-management/security) section.

#### Create database

To create database, navigate to database menu and then click create database. Here you will need to fill in the database name.

#### Create database user

This will create a user for your database. Please note that this user won't be able to use any database that you have until you attach them to any of your database.

For the password, we recommend that you use our Password Generator instead of the password stored in your head. Recommended password length is **more than** 25 characters.

#### Attach user to database

This will attach intended user to the database. Once the user is attached, the user will own the database. You can always revoke the user access from the database.

<div class="alert alert-danger">For the best practice, please make sure each user only owns <strong>one</strong> database.</div>

#### Change database user password

You may change the database password easily using RunCloud. Click on change password and generate your database user password. The new password will be pushed to your server and taken care of by the Agent.

#### Delete database

To delete your database, click on the delete button and fill in the database name. Please make sure there is no Web Application using the database or you will face serious problem since deleting database will drop all your database data which cannot be recovered.
