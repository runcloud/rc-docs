#### Overview

The SSH Key can be used to login to your server without a password. Each user inside server can have multiple SSH keys attached to them. For example, if you want a team member to be able to login to your server under username runcloud, just add another SSH public key to the server. After that, the team member can login to your server without any password.

#### Generating SSH Key

If you do not have any SSH key pair inside your computer, just issue `ssh-keygen -t RSA` inside terminal (Linux & MacOS).

Then you can get the SSH public key by issuing `cat ~/.ssh/id_rsa.pub` which you can register inside RunCloud.

#### Adding SSH Key

To add the SSH Key to your server, you need to fill in the data below:

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Label</td>
            <td>The label for SSH key for you to recognize this key belong to whom</td>
        </tr>
        <tr>
            <td>User</td>
            <td>The user inside your server that you want the ssh key to authenticated to</td>
        </tr>
        <tr>
            <td>Public Key</td>
            <td>The SSH public key get from `cat ~/.ssh/id_rsa.pub`</td>
        </tr>
    </tbody>
</table>

You also can use any SSH Key that you have stored inside [SSH Key Vault](https://runcloud.io/docs/guide/account/ssh-key-vault).

After you have save the key, RunCloud will push the key to the server. And now you can login to your server without password.

#### Deleting SSH Key

Deleting the SSH Key will remove the key from the server. However, the user who owns the key will not be automatically logged out from the server.
