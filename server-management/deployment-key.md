#### Usage

The Deployment Key will allow you to create an ssh-key to deploy your web application using GitHub or Bitbucket. Every system user can only have **one deployment** key and can be used again except in GitHub.

<div class="alert alert-danger">GitHub prevents users from using the same deployment key. The workaround is to create another system user and generate another deployment key.</div>

Please note that when you generate a new key, the old key will be invalid and your **Git Webhook won't be working**.

#### Deployment Key generation rate limit

You can generate deployment keys 10 times a minute. After a minute has passed, you can generate another one. This is to ensure that you are not spamming our service since SSH Key generation is resource-intensive task.
