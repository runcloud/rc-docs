#### What is Cron Job

Cron is a time-based job scheduler in Unix-like computer operating systems. People who set up and maintain software environments use cron to schedule jobs (commands or shell scripts) to run periodically at fixed times, dates, or intervals.<a href="https://en.wikipedia.org/wiki/Cron" target="_blank" rel="nofollow"><sup>[1]</sup></a>

When you set certain command to cron job, it will honour the time that you have set and will run the command according to the time.

#### Creating Cron Job

When creating cron job, you need to specify the data below.

<table class="table table-hover">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Job Name</td>
            <td>Description / reference for the job</td>
        </tr>
        <tr>
            <td>User</td>
            <td>Username of user that execute the job</td>
        </tr>
        <tr>
            <td>Vendor</td>
            <td>Pre-defined binary (can use null value to type full command in command field)</td>
        </tr>
        <tr>
            <td>Command</td>
            <td>The actual command/script to run</td>
        </tr>
        <tr>
            <td>Time of schedule</td>
            <td>Schedule configuration for the job to be executed</td>
        </tr>
    </tbody>
</table>

#### Deleting Cron Job

If you are no longer using the job, you can click the delete button to delete the job that you want. It will have no effect on any other cron job.

#### Rebuilding Cron Job

Rebuilding cron jobs will delete every cron job that you have inside your server including any jobs that were manually written. After that, all jobs will be re-added back to your cron.
