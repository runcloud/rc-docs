#### Server details

The server details are a meta for your server. You can name this server, set the provider of the server, and set the location of your server. This information is for your purposes only.

#### Transfer server

You can use this function to transfer your current server to another registered customer inside RunCloud. This server cannot be transferred to Trial account.

While in the transfer process, you won't be able to use the server until the new owner has accepted or rejected the request. You may also cancel the transfer process before the new owner starts accepting the server.

The transferred server will lose setting for [Slack Notification](https://runcloud.io/docs/guide/server-management/notification#usage), [Telegram Notification](https://runcloud.io/docs/guide/server-management/notification#usage), [Health Notification Setting](https://runcloud.io/docs/guide/server-management/notification#health-notification-setting) and [Backup Instance](https://runcloud.io/docs/backup/summary#rc-docs-scroll).

Once your server has successfully transferred to the new owner, you will no longer be able to manage this server. The transferred server will not be able to be transferred again until 15 days has passed from the last transfer date.

#### Change IP address

You can change your server's IP Address from here. After filling in the form, you need to run a script inside your server to tell RunCloud that you have changed your IP Address.

#### SSH config

SSH Config allows you to configure basic SSH configuration. The configuration is as follows:

<table class="table table-hover">
    <thead>
        <tr>
            <th>Configuration</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Passwordless Login Only</td>
            <td>If you enable this, you can't SSH to your server using password. The only way is to use SSH Key.</td>
        </tr>
        <tr>
            <td>Prevent Root Login</td>
            <td>If you enable this, you can't login as root to your server. It is a really good security measure to enable this function. If next time you want to run as root, disable this function and login as usual.</td>
        </tr>
        <tr>
            <td>Use DNS</td>
            <td>SSH will check for reverse DNS of connecting machine before authentication. If you enable this, it will make SSH authentication to your server becomes slow.</td>
        </tr>
    </tbody>
</table>

#### Auto update

Your server is set to automatically update all packages from RunCloud (NGiNX, Apache2, all PHP version and RunCloud Agent). However, there is another update that you can configure.

<table class="table table-hover">
    <thead>
        <tr>
            <th>Configuration</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Auto Update</td>
            <td>If you enable this function, your server will update everything inside your server. You are <strong>not recommended</strong> to enable this in production server.</td>
        </tr>
        <tr>
            <td>Auto Update Security</td>
            <td>The default config is enable. This will automatically update security release for your server. It is really <strong>recommended</strong> to enable this in both production or staging server.</td>
        </tr>
    </tbody>
</table>
