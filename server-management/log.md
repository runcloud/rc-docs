#### Usage

You can monitor what has happened and what you've done from this section. Logs are categorized into four different sections:

<table class="table table-hover">
    <thead>
        <tr>
            <th>Label</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><span class="label label-success">Info</span></td>
            <td>The server is doing what you are telling it to do.</td>
        </tr>
        <tr>
            <td><span class="label label-info">High Priority Info</span></td>
            <td>The server is doing what you are telling it to do, but it is considered high priority because of deleting or changing something that might affect other section.</td>
        </tr>
        <tr>
            <td><span class="label label-warning">Warning</span></td>
            <td>This is where you should check because it is changing how the server work such as deleting user and stopping some services. Warning label will also raised if RunCloud are unable to connect to your server.</td>
        </tr>
        <tr>
            <td><span class="label label-danger">Error</span></td>
            <td>This label is the one that should have not happen to your server. You need to manually check why this error log raised.</td>
        </tr>
    </tbody>
</table>

While this is a list of actions that have already happened, you can acknowledge them and take preemptive measures in the future.
