#### Monitoring

RunCloud will collect server data every 15 minutes. Information collected from the server is server load, current memory usage, total memory, current hard disk usage, and total hard disk space.

The data is displayed in graphical format which you can view as an Hourly or Daily view.

#### Notification

All health data sent to our server will be processed to check for bad health. You may set a custom benchmark to trigger a notification, but we do recommend these benchmarks:

-   Server load is more than **5**
-   Memory usage more than **85%**
-   Hard disk usage more than **90%**

If the health data matches the criteria you set above,, will will send you an email, Slack notification (if set) and/or Telegram notification (if set) to notify you about your server's health. The notification will be sent only once per hour. We will keep sending you an email, Slack notification and/or Telegram notification until your server is restored to a good health benchmark. If you want to stop the notification, you can disable this feature in the [Notification](https://runcloud.io/docs/guide/server-management/notification) section.

#### Latest health data

The load, memory usage, and disk space usage inside the health section show the average data collected. The latest health data is the latest data that we have received from your server.

#### Disk cleanup

Disk cleanup will wipe your disk space by emptying ALL web app access and error logs, remove ALL rotated logs of the web app, clear apt cache and MySQL bin log that is more than three days. This function is safe to run inside a server that is in production, as no main files within your web app will be deleted.
