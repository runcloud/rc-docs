#### Overview

In this section, you may create a new system user for your server. Creating the user from here will automatically assign the user to the desired group and setting security for the user. The created user can use SFTP to transfer your files to your server.

#### Creating system user

To add a new user, click the add user button and fill in the form. We only require a username to add the user. Password is optional.

<div class="alert alert-danger">Please note that we do not store your password by any means.</div>

#### Changing password

You may change the password for each user from here. Use our Password Generator for maximum password security.

<div class="alert alert-warning">You can't change password for root from here.</div>

#### Deleting system user

You may delete a user from here if you are no longer using the user. Deleting the user will also delete the Web Application associated with the user. If you have any cron job or Supervisord running as the deleted user, you will need to **manually remove** them using the RunCloud panel.
