#### Requirements

Before you can use RunCloud, please make sure your server fulfils these requirements.

Software requirement:

-   Ubuntu 16.04/18.04/20.04 x86_64 LTS (Fresh installation)
-   If the server is virtual (VPS), OpenVZ may not be supported (Kernel 2.6)

Hardware Requirement:

-   More than 1GB of HDD
-   At least 1 core processor
-   512MB minimum RAM
-   At least 1 public IP Address (NAT VPS is not supported)

#### External firewall

For VPS providers such as AWS and AZURE, those providers already include an external firewall for your VPS. Please open port 22, 80, 443, and 34210 to install RunCloud.

#### Installation

Login to your account and click add server. Fill in the form with your server name (any name that you want) and your server IP Address.

<div class="alert alert-danger">Registering the wrong IP Address will prevent your server from downloading the installation script.</div>

SSH into your server as **root** and run the installation script given by RunCloud.

Installation may take up to 5 minutes minimum which may also depend on your server internet speed. After the installation is completed, you are ready to use RunCloud to manage your server.

<!-- ##### Installation video tutorial

Here is the video tutorial on how to install RunCloud inside your server.

<p class="text-center">
    <youtube-video video="tL7DrH_Dcoc" play-text="Watch Installation Video" button-class="btn btn-success btn-lg"></youtube-video>
</p> -->

#### Installation notes on AWS

AWS by default disables root login. To login as root inside AWS, login as default user and then use command `sudo -s`.

```bash
$ ssh ubuntu@<your server IP address>
$ ubuntu@aws:~$ sudo -s
$ root@aws:~# <paste installation script>
```
