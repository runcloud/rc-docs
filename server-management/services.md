#### About

The Services section is where you can control the services inside your server. We only track NGiNX, Apache2, MariaDB, Supervisord, Redis, Memcached and Beanstalkd. You can start, stop, and reload any services that you want anytime.

The services page is cached for 3 minutes. Once you hit stop, start, or reload, then the cache will be removed.

#### Automatic start stop

NGiNX, Apache2 and Supervisord will automatically start and stop depending on usages. If you did not create any Web Application or Supervisord config, the services will automatically stop themselves.

PHP-FPM also follow this start and stop rule. If you are only using a Web Application with PHP7.0, than PHP5.5 and PHP5.6 will stop themselves, so that you are not using more resources than necessary.

#### NGiNX

The front web server. Every static file will be served by NGiNX. Dynamic request will be passed to Apache2.

#### Apache2

The backend web server. Every request to your Web Application will use NGiNX. If NGiNX is unable to serve them, it will be passed to Apache2. Stacking NGiNX in front of Apache2 will enable you to use .htaccess.

PHP will be served by Apache2 using FastCGI/FPM.

#### MariaDB

Your Relational Database Management System (RDBMS). We use MariaDB because it is a fork of MySQL and you can use any third party tools created for MySQL. Also, MariaDB is faster than MySQL (3%-5%).

#### Supervisord

Please check [Supervisord](https://runcloud.io/docs/guide/server-management/supervisord) section.

#### Redis

Redis is a in-memory key-value storage. Redis is best use to store cache data or session. Since Redis stores the data inside your server's memory, it can't be used to store permanent data.

#### Memcached

Memcached is use primarily for storing cache data. Similar to Redis, Memcached stores data inside server's memory.

#### Beanstalkd

Beanstalkd is used to store work queue. You can store your work queue such as sending email and crunching uploaded images to Beanstalkd. You will need to write the logic to pull data from Beanstalkd and running the task in the background.
