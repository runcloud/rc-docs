#### Overview

RunCloud works well with virtual private servers such as Linode, DigitalOcean, Vultr, Amazon Web Services, and Google Cloud Platform. We have tested all five VPSs with RunCloud and they work very well. On this page, we describe these services and go through any quirks each VPS may have with regards to RunCloud.

#### Linode

American privately owned virtual private server (VPS) provider company since 2003. Linode has 9 data centers in 3 regions. Linode SSD VPS starts from USD5/month. Other Linode products and services includes Linode Managed server management, Linode Backup, NodeBalancers load balancer, LongView analysis package, Linode Block Storage.

[Click here to visit Linode](https://www.linode.com/)

#### DigitalOcean (DO)

DigitalOcean (DO) provides an all-in-one cloud platform for developers to run and scale their applications. DO offers SSD VPS (droplet) from USD5/month at 8 different data center regions around the world. Other DO services includes load balancer, floating IP, block storage, object storage, backup & snapshots.

[Click here to visit DigitalOcean](https://www.digitalocean.com/)

#### Vultr

Vultr has largest cloud server network with 15 data centers around the world. Vultr offer SSD VPS (Vultr Cloud Compute) from as low as USD2.50/month. Other Vultr products includes Vultr Bare Metal dedicated cloud server, Vultr Dedicated Cloud, block storage, DDoS protection.

[Click here to visit Vultr](https://www.vultr.com/)

#### Amazon Web Services (AWS)

Amazon Web Services (AWS), offered by Amazon, was launched in 2006, and has since become the world's most popular on-demand cloud computing platform. It provides a mix of Infrastructure as a Service (IaaS), Platform as a Service (PaaS) and packaged Software as a Service (SaaS) offerings.

[Click here to visit Amazon Web Services](https://aws.amazon.com/)

#### Google Cloud Platform (GCP)

Google Cloud Platform (GCP), offered by Google, is a suite of cloud computing services that runs on the same infrastructure as Google search engine and YouTube. All GCP data centers are connected through Google's backbone network, one of the biggest and fastest in the world. GCP offers better pricing than AWS.

[Click here to visit Google Cloud Platform](https://cloud.google.com/)

#### Webdock.io

Webdock.io is a privately owned cloud hosting company based in Odense Denmark, with datacenters in Germany. Webdock VPS container plan starts from only EUR4/month. Webdock includes 8 backup snapshots for each server, and offer free SSL certificate (HTTPS) and free transactional emailing. Webdock comes with a control panel where you can automate most common server tasks. It is in use by agencies managing hundreds of servers across large teams.

[Click here to visit Webdock](https://webdock.io)
