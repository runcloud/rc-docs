#### Server summary

<!-- <img class="img-responsive" alt="Summary page example" src="{{ cdn_url }}/img/docs-server-summary.png?rccbt=1"> -->

Server summary is for viewing your server information. The summary page is cached for 2 minutes, so changes are not always immediate. The summary page will display:

-   Server IP Address
-   Registered server name
-   Registered location
-   Registered provider
-   RunCloud Agent version
-   Kernel version
-   Processor name
-   Available memory / total memory
-   Available disk space / total disk space
-   Server uptime
