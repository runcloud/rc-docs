#### Overview

The web application is your website consisting of files that will be used for functionality and display purposes.

Each web app is separate of each other. Most people who come from cPanel tend to put everything inside a folder and then serve it to the public. Similarly, each web app is separated into its own folder, with its own database and permissions, sandboxed, and served to the public.

Sandboxing each web app allows it to be secure and protected in its own right. If one of your websites is compromised, the others would be completely safe.

With your Web Application, you can still put everything under one folder, but we highly recommend keeping everything separate for security reasons.

Why do we ask you to keep your web apps separate? Every app is separated from each other because each app contains its own settings, database, and permissions.

#### Creating web application

Creating Web Applications is easier than you can imagine. Click the create button and you will see a form to create your Web Application.

<table class="table">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Web Application Name</td>
            <td>Name of your website or any name you choose.</td>
        </tr>
        <tr>
            <td>Domain Name</td>
            <td>The domain or subdomain name to access to your website. Your domain does not have to be active to set up a server. You may use the IP address of your server for access once you set the web application as the <a role="button" @click="scrollTo('#default-web-application')">Default Web Application</a>. As of December 21, 2019, we are providing free domains for you to use for testing purposes.</td>
        </tr>
        <tr>
            <td>User</td>
            <td>Create a user associated with the website. Every file inside the web application will belong only to this user.</td>
        </tr>
        <tr>
            <td>Public Path</td>
            <td>This is where your visitors can see your website. By default, index.php will usually be the default file in the folder that is publicly seen on the web. For most web applications, you can leave this blank. If you are using a custom web application such as Laravel, this will point to /path/to/root/web/application/public</td>
        </tr>
        <tr>
            <td>PHP Version</td>
            <td>You can run your website with any PHP version that you want. If your web application breaks at PHP7.0, PHP 7.1 or PHP7.2, you can revert to an older version such as PHP5.5 or PHP5.6.</td>
        </tr>
        <tr>
            <td>Web Application Stack</td>
            <td>
                There are <strong>two</strong> stack that you can choose from here.
                <br>
                <br>
                <ol>
                    <li>NGINX + Apache2 Hybrid Stack:<br>
                    NGINX works as reverse proxy for Apache2. Different from most setups, this stack doesn't fully proxy Apache2. NGINX is really best at serving static content. By using this stack, your static files (eg: css,js,images,fonts) will be served by NGINX. If it is PHP, NGINX will pass it to Apache2 and the request will be passed to PHP-FPM. This setup is great if you are leveraging with <strong>.htaccess</strong>
                    </li>
                    <li>Native NGINX Stack:<br>
                    Your sites will be fully controlled by NGINX. For PHP files, NGINX will pass it to FastCGI to communicate with PHP-FPM. This stack <strong>doesn't support .htaccess</strong>. If you are going to rewrite or including your own config, you can extend NGINX config by following <a role="button" href="/docs/guide/cheat-sheet/nginx#custom-nginx-config">this example</a>.
                    </li>
                    <li>Native NGINX + Custom config Stack:<br>
                    Your sites will be fully controlled by NGINX. This stack will not serve your PHP file. You have to manually extend the config by following <a role="button" href="/docs/guide/cheat-sheet/nginx#custom-nginx-config">this example</a>. This is suitable if you want to run nodejs/python/golang/websocket/ror/etc.
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Stack Mode</td>
            <td>
                There are <strong>two</strong> stack mode that you can choose from here.
                <br>
                <br>
                <ol>
                    <li>Production<br>
                    This will instruct the visitor to your web application to cache static files (js/css/fonts/images/html) inside their browser.
                    </li>
                    <li>Development
                        This will prevent the browser from caching your static files (js/css/fonts/images/html). Development mode <strong>does not prevent</strong> ther caching methods such as Redis, Memcached or in-app caching.
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Advance Settings</td>
            <td>
                Please take a look at <a href="#web-application-settings">Web Application Settings</a> section.
            </td>
        </tr>
    </tbody>

</table>

After you have created a web application, we will send a signal to your server to create the  web application which will enable the provision of further changes.

#### Default web application

You can set your web application as default web application. If you set your web application as default, every DNS result that is pointing to your server will go to this web application if there is no matching app name inside your web app. If a visitor tries to access your server by using the IP Address, this web app will be served to the user.

#### Web Application Alias

For each Web Application, you can create Web Application Alias. The Web Application Alias allows you to create different Web Application but points to the same folder. For example, if you create Web Application `awesomeapp`, the root folder of the Web Application will be, `/home/runcloud/webapps/awesomeapp`. But if you create an alias for this Web Application and name it `otherapp`, the root folder will also be `/home/runcloud/webapps/awesomeapp`.

The Web Application Alias will not have **Git**, **Script Installer** and **File Manager** menu since it is an alias to another Web Application.

The usage of this Web Application Alias is to allow you to have multiple domains per Web Application with multiple SSL Certificates.

For example, if your Web Application uses Microservice Architecture and depends on [Consul](https://www.consul.io/), you will have non-existing domain name, example.consul. But now, the Microservice Web Application also have public facing domain name, app1.example.com. If you are using both of these domain name (example.consul and app1.example.com) inside the same Web Application, you will not be able to retrieve SSL Certificate from Let's Encrypt. By creating the Web Application Alias, app1.example.com will be inside one Web Application and example.consul will be on the alias.

You also can create Web Application Alias to act as a subdomain that points to another folder inside your Web Application. But this setup is **highly discourage**.

#### Web application summary

This is the summary of your web application. It will show you data related to your web app. Below is the full description of the summary section.

<table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Owner</td>
            <td>The username of the user who owns the web application.</td>
        </tr>
        <tr>
            <td>Total Domain Name</td>
            <td>Total domains connected to this web application.</td>
        </tr>
        <tr>
            <td>PHP Version</td>
            <td>PHP version of this web application.</td>
        </tr>
        <tr>
            <td>Web Application Stack</td>
            <td>Stack that you use. Either NGINX + Apache2 Hybrid, Native NGINX, Native Nginx + Custom config stack.</td>
        </tr>
        <tr>
            <td>Root Path</td>
            <td>The root folder of your Web Application. PHP can be executed inside this directory only.</td>
        </tr>
        <tr>
            <td>Public Path</td>
            <td>Path to the public folder that your visitors can access.</td>
        </tr>
        <tr>
            <td>Total data transfer yesterday</td>
            <td>Total amount of data that your website served yesterday.</td>
        </tr>
        <tr>
            <td>Total data transfer this month</td>
            <td>Total amount of data that your website served this month.</td>
        </tr>
        <tr>
            <td>SSL Provider</td>
            <td>Let's Encrypt or custom SSL provider.</td>
        </tr>
        <tr>
            <td>Git Provider</td>
            <td>The name of git repository provider that you are using.</td>
        </tr>
        <tr>
            <td>Repository</td>
            <td>Git repository that you are using.</td>
        </tr>
        <tr>
            <td>Repository URL</td>
            <td>URL to your git repository from your git provider.</td>
        </tr>
        <tr>
            <td>Branch</td>
            <td>Current git branch that you are using.</td>
        </tr>
    </tbody>
</table>

#### Transfer stats

This is the graph that shows you how much data you have served to your visitors. Calculations are in MB. Your server will send the Web Application transfer stats every 12 hours. This section allows you to view transfer data in Daily and Monthly views.

#### Git

##### Overview

One of the best features of RunCloud is that you can use git to attach your project to your Web Application. You can do an automatic pull to sync your website with your code. You can also change your git branch on the fly. If you are using git, you won't be able to use Script Installer inside the Web Application.

##### Attaching git repository

The first step is to attach the git repository to your Web Application and generate a [Deployment Key](https://runcloud.io/docs/guide/server-management/deployment-key). Please make sure that you are generating the key with the **same user** as your Web Application owner.

You will need to fill in information about your git repository to attach it to your Web Application:

<table class="table">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Provider</td>
            <td>The git provider which can be a Custom git Server, Bitbucket, GitHub GiTLab and also Self Hosted GitLab.</td>
        </tr>
        <tr>
            <td>Git User</td>
            <td>Only necessary when using a custom git server. This indicates the user running the git server. Usually the user name is <strong>git</strong>.</td>
        </tr>
        <tr>
            <td>Git Host</td>
            <td>Domain name of the git server. E.g: versioncontrol.example.com. You must have a FQDN as the IP Address isn't supported yet.</td>
        </tr>
        <tr>
            <td>Repository</td>
            <td>The repository you want to attach. For example if you use GitHub, your repository URL would be <strong>https://github.com/coolcodemy/test</strong>. Then you just have to add <strong>coolcodemy/test</strong> to the repository field.</td>
        </tr>
        <tr>
            <td>Branch</td>
            <td>The repository branch that you want to us, which is usually the <strong>master</strong> branch.</td>
        </tr>
    </tbody>
</table>

<div class="alert alert-success">When you add git repository to your Web Application, please wait until it is attached to your Web Application before navigating away from the page.</div>

### Git WebHook

After you have attached your git repository, you will get a WebHook URL. You can register this URL to a custom Git Server, Bitbucket, GitHub, GitLab or Self Hosted GitLab repository to enable automatic pull your commit from your origin.

All you have to do is copy the link, add it to the same repository you have registered to your WebApplication, and enable it.

Your Web Application will automatically pull new commit everytime you push your code to your repository.

If you are using GitHub, please make sure that you are using **application/json** as Webhook Type. We don't support **application/x-www-form-urlencoded**.

##### WebHook history

This is the history of your commit that RunCloud received from the WebHook.

##### Deployment errors

Starting with agent version 2.0.0 you will be able to view **stdout** and **stderr** of your deployment error log. This error log as the name says, will only available for viewing if your deployment is failed.

The **stdout** and **stderr** will be pushed to Slack and/or Telegram (if it set inside <a role="button" href="/docs/guide/server-management/notification">notification section</a>) and also to your registered email.

The error log will be automatically cleared from the panel after a week.

##### Changing Git branch

You can change your Git branch from here. Please note that if you put the wrong branch, you will see an error inside the server's log.

You can create two web application and use branch master as the production and development branch for staging server.

##### Deployment script

When the webhook receives a push request, you might want to run a custom script to clear some cache or upgrade your web application. You can write a script to do this command and you must include your own **git merge** command.

<div class="alert alert-success">Deployment script is a <strong>bash script</strong> but you don't have to include #!/bin/bash at the top of your deployment script.</div>

This script will be terminated after 10 minutes if it does not stop by itself. This is to prevent your server from being flooded with a script that does not stop by itself.

You must tick **Automatically run this script upon new commit push** for the script to work.

The script will be running inside your **Web Application root folder**. If your deployment script includes composer and php command, it will use default [PHP-CLI](https://runcloud.io/docs/guide/server-management/php-cli). If you want to run a script using a different PHP version, you must use replacer.

###### Replacer

Replacer is where you can tell the deployment script to run specific php version. E.g: `{php70rc}` will run PHP7.0 from /RunCloud/Packages/php70rc/bin/php.

You can always type full path to PHP version if you don't want to use replacer.

###### Manual Git deploy

To manually deploy your script, fill in the command inside your deployment script, click save and then deploy. Your server will follow the command from your script.

#### Script installer

The Script installer allows you to install popular PHP scripts to your Web Application. Please note that Script Installer only downloads the files for you and places it inside your Web Application folder. For the installation, you will have to do it yourself. Installing a script using Script Installer will prevent you from using <a href="#git">Git</a>..

<!-- ##### Video tutorial on installing WordPress using Script Installer

Here is the video tutorial on how to install WordPress using RunCloud.

<p class="text-center">
    <youtube-video video="y3Y4oCwNtQc" play-text="Watch how to easily install WordPress on RunCloud" button-class="btn btn-success btn-lg"></youtube-video>
</p> -->

#### File manager

The File Manager allows you to control the files inside your Web Application from the RunCloud panel. You can add, edit, delete, change permissions, and rename files and folders using the RunCloud File Manager.

<div class="alert alert-danger">Only you have access to your files inside your server.</div>

Navigating through file manager is easy. To select multiple files or folder, click the name of the file or folder that you want to open. To enter the folder, double click the folder name. To create new file or folder, click the button to create file or folder and insert the name of your file or folder. The file or folder that you have created will reside inside your current directory and you will be able to edit files and folders directly.

##### Cloud editor

You can edit your files using RunCloud Cloud Editor. You can click one file and then click View/Edit to view your file. From there, you can edit your file easily. You can change the theme of the file editor to something that suits your preference. The file editor theme will be saved for the next time you open up another file to edit. If you are trying to edit a tracked git file, the editor will prevent you from saving the file.

#### Domain name

You can add or delete domain name from here. Please make sure that your domain name is pointing to your server inside your DNS provider. After you have added the domain name, you can open your website using any domain name that you have registered inside RunCloud.

Deleting domain name will also remove it from your Web Application. You won't be able to access your website using the domain name again until you add it back.

#### SSL/TLS

Using RunCloud, you can easily deploy your Web Application to use SSL/TLS. The configuration for SSL/TLS will get you the A+ configuration and comply with PCI-DSS, HIPAA and NIST as you can check from <a href="https://www.htbridge.com/ssl/" target="_blank" rel="nofollow">here</a>.

You can use free SSL/TLS from Let's Encrypt or use your own certificate and key.

You can set whether to allow your website visitor to always use HTTPS or enable HTTP access to them. You can also strictly enable HTTPS using <a href="https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security" target="_blank" rel="nofollow">HSTS</a>.

Adding SSL/TLS to your web application will have another benefit of secure communication between visitor and the server. You will get HTTP/2 and Brotli compression which comes built-in with RunCloud.

##### Let's Encrypt

Generating Let's Encrypt certificate is easier using RunCloud. There are **two ways** to authorize Let's Encrypt, **http-01** and **dns-01**.

If you are adding new domain name to your Web Application, you need to **redeploy** the Let's Encrypt to make it valid.

Let's Encrypt Certificate is valid for 3 month periods and must be renewed, which is done automatically by RunCloud.

Now lets talk about each authorization method.

###### http-01

This method will create authorization file(s) inside your server. Let's Encrypt will attempt to check whether this/these file(s) exists or not. If it isn't exists, the authorization challenge will fail.

This authorization method will check the existence of the files(s) based on **A**, **AAAA** or **CNAME** DNS record. If the DNS record does not pointing to your server, the challenge will fail.

http-01 authorization method does not support **wildcard ssl**.

###### dns-01

This method will create a DNS **TXT** record(s) inside your DNS service provider. With this authorization method, Let's Encrypt will try to check the existence of the TXT record(s). If the record(s) exists, the challenge will be valid.

To use this authorization method, you need to supply [3rd Party API](https://runcloud.io/docs/guide/account/3rd-party-api-key) to use. Currently we only support **Cloudflare**, **Linode** and **Digital Ocean** DNS.

dns-01 authorization method does support **wildcard ssl**.

<!-- ##### Video tutorial on requesting Let's Encrypt SSL/TLS certificate

Here is the video tutorial (**old tutorial**) on how to request Let's Encrypt certificate using RunCloud.

<p class="text-center">
    <youtube-video video="c-cSDjooYc0" play-text="Watch how to install Let's Encrypt" button-class="btn btn-success btn-lg"></youtube-video>
</p>
 -->
##### Custom SSL/TLS provider

Using custom SSL/TLS provider is straight forward. You just have to buy certificate either from Comodo, Verisign or any SSL provider. After that, add your certificate and key to RunCloud. We will automatically configure them for you.

When you need to change you certificate, just replace the old certificate value with the new one and click save.

#### Web application settings

This section contains two section which is PHP Version selection and Web Server Setting.

##### PHP version

Here you can change your Web Application PHP version with just one click.

##### Web server configuration

The web server configuration consists of several section:

###### Public path

This is where you can change your web application public path. For example if you are using PHP framework, you can change your public path to /path/to/framework/public

###### Web application stack

There are <strong>two</strong> stack that you can choose from here.

1. NGINX + Apache2 Hybrid Stack - NGINX works as reverse proxy for Apache2. Different from most setups, this stack doesn't fully proxy Apache2. NGINX is really best at serving static content. By using this stack, your static files (eg: css,js,images,fonts) will be served by NGINX. If it is PHP, NGINX will pass it to Apache2 and the request will be pass to PHP-FPM. This setup is great if you are leveraging with **.htaccess**.

2. Native NGINX Stack - Your sites will be fully controlled by NGINX. If it is PHP files, NGINX will pass it to FastCGI to communicate with PHP-FPM. This stack **doesn't support .htaccess**. If you are going to rewrite or including your own config, you can extend NGINX config by following [this example](https://runcloud.io/docs/guide/cheat-sheet/nginx#custom-nginx-config).

3. Native NGINX + Custom config Stack- Your sites will be fully controlled by NGINX. This stack will not serve your PHP file. You have to manually extend the config by following [this example](https://runcloud.io/docs/guide/cheat-sheet/nginx#custom-nginx-config). This is suitable if you want to run nodejs/python/golang/websocket/ror/etc.

###### Web application stack mode

There are <strong>two</strong> stack mode that you can choose from here.

1. Production -This will instruct visitor to your web application to cache static files (js/css/fonts/images/html) inside their browser.
2. Development <br> This will prevent the browser from caching your static files (js/css/fonts/images/html). Development mode **does not prevent** caching method such as Redis, Memcached or in-app caching.


##### Web application type

You can change the type of your Web Application here to enable custom menu for your Web Application. For example, if you choose **WordPress** as your Web Application type, it will enable a hidden menu only for working with WordPress.


###### NGiNX

This is the configuration for your NGiNX block for the Web Application. Changing this value will have no effect on other Web Application that you own.

<table class="table">
    <thead>
        <tr>
            <th>Checkbox</th>
            <th>Reference Link</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Clickjacking Protection</td>
            <td><a href="https://en.wikipedia.org/wiki/Clickjacking" target="_blank" rel="nofollow">https://en.wikipedia.org/wiki/Clickjacking</a></td>
        </tr>
        <tr>
            <td>XSS Protection</td>
            <td><a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection" target="_blank" rel="nofollow">https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection</a></td>
        </tr>
        <tr>
            <td>MIME Sniffing Protection</td>
            <td><a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options" target="_blank" rel="nofollow">https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options</a></td>
        </tr>
    </tbody>
</table>

###### FPM

This is the configuration for PHP-FPM settings for this Web Application. It will have no effect on other Web Application.

<table class="table">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Process Manager</td>
            <td>How the PHP-FPM run. There are <strong>three</strong> pm settings that you can choose which is <i>ondemand</i>, <i>dynamic</i> and <i>static</i>. Our default settings use <i>ondemand</i> because it will scale depending on number of visitors you have on your website. Read <a href="https://ma.ttias.be/a-better-way-to-run-php-fpm/" target="_blank" rel="nofollow">here</a> for more info</td>
        </tr>
        <tr>
            <td>pm.start_servers</td>
            <td>You can only set this if pm is set to <i>dynamic</i>. When you start the FPM process, it will start with this number of process</td>
        </tr>
        <tr>
            <td>pm.min_spare_servers</td>
            <td>The desired minimum number of idle FPM processes. Used only when pm is set to <i>dynamic</i></td>
        </tr>
        <tr>
            <td>pm.max_spare_servers</td>
            <td>The desired maximum number of idle FPM processes. Used only when pm is set to <i>dynamic</i></td>
        </tr>
        <tr>
            <td>pm.max_children</td>
            <td>Allowed number of process for PHP-FPM to fork</td>
        </tr>
        <tr>
            <td>pm.max_request</td>
            <td>The number of request can each PHP-FPM child serves</td>
        </tr>
    </tbody>
</table>

###### PHP

This is the PHP configuration for your Web Application. It will have no effect on other Web Application. It will also have no effect on [PHP-CLI](https://runcloud.io/docs/guide/server-management/php-cli).

<table class="table">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>open_basedir</td>
            <td>The maximum parent folder that the web application can execute. PHP won't be executing outside of this folder. If you have to change this setting, there is something unusual going on inside your web application. It is safe to say that you code your web application wrongly.</td>
        </tr>
        <tr>
            <td>data.timezone</td>
            <td>The timezone that will be used inside your web application. If you have set your timezone inside the profile, this value will follow your current timezone.</td>
        </tr>
        <tr>
            <td>disable_function</td>
            <td>PHP function that should be disabled for security reasons. If you need to use one or more of this php function, do not blindly remove all function. You might pose high security vulnerability if you remove everything from here.</td>
        </tr>
        <tr>
            <td>max_execution_time</td>
            <td>The maximum number of seconds that PHP script can execute. PHP script will stop itself after reaching this time.</td>
        </tr>
        <tr>
            <td>max_input_time</td>
            <td>The maximum number of seconds that a script allows to receive input from website visitor such as uploading picture. If your website host video uploading site, you might need to increase this value.</td>
        </tr>
        <tr>
            <td>max_input_vars</td>
            <td>The maximum number of variables can a request handle.</td>
        </tr>
        <tr>
            <td>memory_limit</td>
            <td>The maximum amount of memory (in MB) that a script allowed to use.</td>
        </tr>
        <tr>
            <td>post_max_size</td>
            <td>The maximum size of post request (in MB) your server can handle.</td>
        </tr>
        <tr>
            <td>upload_max_filesize</td>
            <td>The maximum size of uploaded file your Web Application can handle. Usually same or lower than post_max_size.</td>
        </tr>
        <tr>
            <td>session.gc_maxlifetime</td>
            <td>The maximum number of seconds your PHP session can be valid before garbage collector delete your session.</td>
        </tr>
        <tr>
            <td>allow_url_fopen</td>
            <td>Allow your php script to open remote files from another server. The default value is on, but if you don't use it, you may disable this setting.</td>
        </tr>
    </tbody>
</table>

#### Web Server Log

This is where you can see the Web Server error and access log. Depending on your Web Application stack, if you are using Apache2 + Nginx Hybrid stack, you can access Nginx Access Log, Nginx Error Log, Apache2 Access Log and Apache2 Error Log. While using Native Nginx or Native Nginx + Custom config stack, you can only see Nginx Access Log and Nginx Error Log.

#### Log

This is your Web Application log. Most of Web Application log will be registered in here.

#### WordPress - RunCache

RunCache is the full page caching mechanism that we have implemented inside RunCloud. RunCache will run before any other caching mechanism that you have implemented. By using RunCache, your website will be displayed in a static page without having to regenerate it by PHP.

RunCache installation will involve **two** things. NGiNX config and RunCache Purger WordPress plugin. The plugin is needed when you wanted to clear the cache from the WordPress admin. The plugin will automatically purge the cache when you update, delete, create a post or a comment.

#### WordPress - General Settings

Inside the general setting, you can update the basic WordPress configuration. What we have implemented for now is just the ability to change the WordPress address and site address. We will be adding more features on this later.